import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Window 2.2

ApplicationWindow {
    visible: true
    width: 500
    height: 600
    title: qsTr("Tabs")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Redactirovanie {
            id: redactirovanie
        }


        Predprosmotr {
            id: predprosmotr
        }

        Basket {
            id: basket
        }


        Drawer{
            id: dr1
            width: parent.width * 0.7
            height: parent.height
            GridLayout{
                columns: 1
                Text {
                    text: qsTr("Магазин")
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 20
                    font.bold: true
                  //  color: "#e6e6e6"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15
                    text: qsTr("Экзаменационное задание по дисциплине<br>
                                'Разработка безопасных мобильных приложений'<br>
                                Московский Политех<br>
                                13 июля 2020 г")
                }
                Image {
                     anchors.horizontalCenter: parent.horizontalCenter
                    source: "qrc:/images.png"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15
                    text: qsTr("Автор: vikatashnova@gmail.com")

                }

                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15
                    text: '<html><style type="text/css"></style><a href="https://gitlab.com/Viktoriya_cv/181_331_tashnova">https://gitlab.com/Viktoriya_cv/181_331_tashnova</a></html>'

                }

            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Каталог")
        }

        TabButton {
            text: qsTr("Описание")
        }

        TabButton {
            text: qsTr("Корзина")
        }

    }
}
