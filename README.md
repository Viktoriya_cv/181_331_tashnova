# 181_331_Tashnova
# Описание

Экзамен 13 июля 2020г по дисциплине "Программирование безопасных мобильных приложений"
Разработать мобильное приложение - онлайн-Магазин. Графический интерфейс содержит 3 страницы: 
"Каталог", "Описание товара" и "Корзина", а также  боковую панель (Drawer) с реквизитами и описанием проекта.

Данный код - экзаменационная работа, выполенная Ташновой Викторией Александровной, учебная группа 181-331

Моя почта:
vikatashnova@gmail.com

Сайт университета: 
mospolytech.ru

# Снимки

<img src="https://sun1-26.userapi.com/zqiZGPO94mgqLlmR3rmH3HGozV_r6SOF27PdeQ/VessLOc00hg.jpg" width=300> <img src="https://sun1-22.userapi.com/HbZnkeHSFo0-2lSm3tfw2Hb2EpPUfWyUMA4UBQ/3_lRvsMnq9g.jpg" width=300> <img src="https://sun9-26.userapi.com/8nuHMe93qBJMarI98fMj9SMlTHVfYW_0AtZ0rg/4-qKmaHn0dY.jpg" width=300>  

# О создании приложения
Приложение созданно в среде разработки QT, в качестве экзаменационной работы в формате WordSkils. 
Работа выполнялась 13.07.2020