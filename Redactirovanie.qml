import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3

Page {
    id:redactirovanie
    header:
        Item {
        width: parent.width
        height: 30
        Rectangle {
            anchors.fill: parent
            color: "#e6e6e6"
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                height: parent.height
                width: parent.height
                id:btn_menu
                Rectangle {
                    anchors.fill: parent
                    color: "#e6e6e6"
                    Image {
                        height: 25
                        width: 25
                        source: "qrc:/open-menu.png"

                    }
                }

                onClicked: {
                    dr1.open();
                }


            }

            Text {
                id: rtcg_h
                text: qsTr("Каталог")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                font.bold: true
            }
        }
    }

    RowLayout{
        id:switch_row
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            text: _switch.checked ? "Список":"Таблица"
            color: "#8a8a8a"
            font{
                family: "Tahoma"
                pixelSize: 20
            }
        }

        Switch {
            id:_switch
        }
    }

    Rectangle {
        id: field
        anchors.margins: 10
        anchors.top: switch_row.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: control.top
        color: "#e6e6e6"
        radius:10

        GridView{
            id:lw
            visible:true
            anchors.fill: parent
            anchors.margins: 5
            cellWidth: _switch.checked ? field.width/2 : field.width/2.1
            cellHeight: _switch.checked ? field.width/3 : field.width*0.3

            delegate: Rectangle{
                color:"white"
                radius: 15
                anchors.margins: 3
                height: lw.cellHeight/1.5
                width: lw.cellWidth

                ColumnLayout {
                    anchors.fill: parent
                    RowLayout {
                        Rectangle {
                            id:tovar
                            height: 70
                            width: 70

                            Image {
                                id: image
                                source: photo
                                width: tovar.width
                                height: tovar.height

                            }
                        }

                        ColumnLayout {
                            Label {
                                text: name
                                font.pixelSize: 20
                                font.bold: true
                            }

                            Label {
                                text: count
                                font.pixelSize: 15
                                font.bold: true
                                color: "#9f9f9f"
                            }
                        }

                        Button{

                            icon.source: "qrc:/shopping-basket.png"
                            icon.color: "transparent"

                            background: Rectangle {
                                color: "royalblue"
                                radius: 15
                            }
                        }
                    }
                }

            }

            model: ListModel //модель из с++ на основе QabstractListModel
            {
                ListElement{
                    photo: "qrc:/books.png"
                    name:"Книга"
                    count: "500"
                }

                ListElement{
                    photo: "qrc:/news.png"
                    name:"Журнал"
                    count: "200"
                }

                ListElement{
                    photo: "qrc:/notepad.png"
                    name:"Тетрадь"
                    count: "50"
                }
            }
        }
    }

    Button{
        id: control
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 5

        background: Rectangle {
            implicitWidth: 250
            implicitHeight: 45
            opacity: enabled ? 1 : 0.3
            color: "royalblue"
            radius: 15
        }
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: img.left
            color: "white"
            text: qsTr("Перейти в корзину")
            font{
                family: "Tahoma"
                pixelSize: 20
            }
        }

        onClicked: {
            swipeView.currentIndex = 2
        }

    }
}
