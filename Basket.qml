import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Window 2.2

Page{
    id:basket

    header:
        Item {
        width: parent.width
        height: 30
        Rectangle {
            anchors.fill: parent
            color: "#e6e6e6"
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                height: parent.height
                width: parent.height
                id:btn_menu
                Rectangle {
                    anchors.fill: parent
                    color: "#e6e6e6"
                    Image {
                        height: 25
                        width: 25
                        source: "qrc:/back (1).png"
                    }
                }

                onClicked: {
                    swipeView.currentIndex = 0
                }
            }

            Text {
                id: rtcg_h
                text: qsTr("Корзина")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                font.bold: true
            }
        }
    }

}
