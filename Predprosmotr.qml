import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtWebView 1.1
import QtWebEngine 1.10
import QtQuick.Window 2.0


Page {
    id:predprosmotr
    header:
        Item {
        width: parent.width
        height: 30
        Rectangle {
            anchors.fill: parent
            color: "#e6e6e6"
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                height: parent.height
                width: parent.height
                id:btn_menu
                Rectangle {
                    anchors.fill: parent
                    color: "#e6e6e6"
                    Image {
                        height: 25
                        width: 25
                        source: "qrc:/back (1).png"
                    }
                }

                onClicked: {
                    swipeView.currentIndex = 0


                }
            }

            Text {
                id: rtcg_h
                text: qsTr("Описание товара")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                font.bold: true
            }
        }
    }

    Button{
        id: control
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 5

        background: Rectangle {
            implicitWidth: 250
            implicitHeight: 45
            opacity: enabled ? 1 : 0.3
            color: "royalblue"
            radius: 15
        }
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
            text: qsTr("Добавить в корзину")
            font{
                family: "Tahoma"
                pixelSize: 20
            }
        }
    }

}
